
ACCOUNT=966756065099
REGION=us-east-1
SECRET_NAME=docker-pass
EMAIL=arpit@xyz.com
aws ecr --region us-east-1 get-authorization-token --output text --query 'authorizationData[].authorizationToken' --profile arpit_cred | base64 -d | cut -d: -f2 > token
TOKEN=`cat token`


kubectl delete secret --ignore-not-found $SECRET_NAME  --namespace=$1
kubectl create secret docker-registry $SECRET_NAME \
 --docker-server=https://${ACCOUNT}.dkr.ecr.${REGION}.amazonaws.com \
 --docker-username=AWS \
 --docker-password="${TOKEN}" \
 --docker-email="${EMAIL}" \
 --namespace=$1

 rm token

